#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h> // toupper() and tolower()

#define MAX_ALB 500
#define MAX_ART 200

#include <windows.h>
#include "resource.h"

const char g_szClassName[] = "myWindowClass";
const char g_szAddClassName[] = "myAddAlbWindowClass";

HWND hAlbLabel;
HWND hAlbListBox;
HWND hArtLabel;
HWND hArtListBox;
HWND hAlbDtLabel;
HWND hAlbDtListBox;
HWND hAlbAddBtn;
HWND hArtAddBtn;

HWND hAlbDetBtn;
HWND hArtDetBtn;

HBRUSH g_hbrBackground;
HFONT hDefault;

HWND g_hMainWindow;
HWND hAddAlbWnd;
HWND hAddArtWnd;

HWND hAlbNameLbl, hAlbArtNameLbl, hAlbRelDateLbl;
HWND hAlbName, hAlbArtName, hAlbRelDate;
HWND hArtNameLbl, hArtAlbNameLbl;
HWND hArtName, hArtAlbName;

HWND hAlbAddButton, hArtAddButton;

struct Artist {
	int key;
	char name[50];
	char bday[20];
	char country[50];
	int age;
	int albumids[MAX_ALB]; /** We store album keys in the artist struct */ /* There needs to be function to save the keys to file */
	char **album_names;
	int alb_count; /** This number keeps increasing when a new album is created as well as when an album is mapped to the artist */

} artist;

struct Album {
	int key;
	char name[50];
	struct Artist artst;
	char release_date[50];
} album;

struct Catalog {
	int key;
	struct Album albums[MAX_ALB];
	struct Artist artists[MAX_ART];
	char storename[50];
	char open_date[50];
	int alb_count;
	int art_count;
	/* A current key of artist is prefered */
	int art_current_key;
	/* A current key of album is prefered */
	int alb_current_key;
} catalog;

void strip_newline(char *str, int len) {
	int i = 0;
	for(i=0; i<len; i++) {
		if(str[i] == '\n') {
			str[i] = '\0';
			break;
		}
	}
}

void show_albums_(struct Catalog *cat) {
	int i = 0;
	if(cat->alb_count > 0) {
		for(i=0; i<cat->alb_count; i++) {
			printf("%d) %s: By %s\n", i+1, cat->albums[i].name, cat->albums[i].artst.name);
		}
	} else {
		printf("No albums created yet!\n");
	}
}

int get_artist_key_(struct Catalog *cat, char *artist_name) {
	int i = 0;
	int key = -1;
	for(i=0; i<cat->art_count; i++) {
		if(strcmp(cat->artists[i].name, artist_name) == 0) {
			key = i;
			break;
		}
	}
	if(key > -1)
		cat->art_current_key = key;
	return key;
}

/**
* @abstract Add an album to the catalog.
* @return void
* @param struct Catalog pointer
* @var char* int FILE*
*/
void add_album_(struct Catalog *cat) {
	FILE *fp;
	char input[50];
	char albumname[55];
	char artstname[50];
	int key;
	printf("Add a new album (type 'exit' to quit): ");
	while(fgets(input, 50, stdin)) { // Loop through the option to grab as many albums as possible
		strip_newline(input, 50); // Strip out any newlines from input
		if(strlen(input) > 0) { // See if user entered any input
			if(strcmp(input, "exit") != 0) { // If user didn't type 'exit', we proceed

				/** Copy the input into the catalog's album struct */
				strcpy(cat->albums[cat->alb_count].name, input);
				fp = fopen("albums.txt", "a+");
				if(fp) {
					fprintf(fp, "%s\n", cat->albums[cat->alb_count].name);
					fclose(fp);
				}
				memset(albumname, 0, sizeof(albumname));
				strcat(albumname, cat->albums[cat->alb_count].name);
				strcat(albumname, ".txt");
				albumname[strlen(albumname)+1] = '\0';
				fp = fopen(albumname, "w"); // Information saved into a file
				
				/** Reading artist information */
				printf("Enter the artist name ('exit' to quit): ");
				memset(input, 0, sizeof(input));
				fgets(input, 50, stdin);
				strip_newline(input, 50);
				if(strlen(input) > 0) {
					
					if(strcmp(input, "exit") != 0) {
						/** A check to ensure the artist doesn't exist already in the application */
						
						strcpy(cat->albums[cat->alb_count].artst.name, input);
						if(fp) {
							fprintf(fp, "Artist: %s\n", cat->albums[cat->alb_count].artst.name);
							fclose(fp);

							strcpy(cat->artists[cat->art_count].name, input);

							memset(artstname, 0, sizeof(artstname));
							strcat(artstname, input);
							strcat(artstname, ".txt");
							artstname[strlen(artstname)+1] = '\0';

							if(!artist_exists_(cat, input)) { // Create new artist and map album to him
								cat->artists[cat->art_count].albumids[cat->artists[cat->art_count].alb_count] = cat->alb_count; // Assign album key to artist
								++cat->artists[cat->art_count].alb_count; // Albums count => How many albums the artist has..
								fp = fopen("artists.txt", "a+");
								if(fp) {
									fprintf(fp, "%s\n", cat->artists[cat->art_count].name);
									fclose(fp);
								}

								fp = fopen(artstname, "w");
								if(fp) {
									fprintf(fp, "Name: %s\n", input);
									fclose(fp);
								}
								cat->art_current_key = cat->art_count;
								++cat->art_count;

							} else { // Start mapping. Still to be done..
								printf("Artist name already exists. Mapping album to artist..\n");

								/** Get key of existing artist and map album to his album keys */
								/* key is equal to cat->art_count */
								key = get_artist_key_(cat, input);
								if(key > -1) {
									cat->artists[key].albumids[cat->artists[key].alb_count] = cat->alb_count; // Assign album key to artist in memory
									++cat->artists[key].alb_count;
									printf("Album mapped to artist successfully..\n");
								}

								/** We do not increment artist count as we already have, and are mapping */
							}

							/** Save album id to artist integer array */
							fp = fopen(artstname, "a+");
							if(fp) {
								fprintf(fp, "Album ID: %d\n", cat->alb_count); // Assign album key to artist in file
								fclose(fp);
							}
						}
					}
				}

				printf("Enter the album's release date (dd-mm-yy): ");
				memset(input, 0, sizeof(input));
				fgets(input, 50, stdin);
				strip_newline(input, 50);
				if(strlen(input) > 0) {

					if(strcmp(input, "exit") != 0) {
						strcpy(cat->albums[cat->alb_count].release_date, input);
						fp = fopen(albumname, "a+");
						if(fp) {
							fprintf(fp, "Release date: %s\n", cat->albums[cat->alb_count].release_date);
							fclose(fp);

						}
					}
				}
				cat->alb_current_key = cat->alb_count;
				++cat->alb_count;

				show_albums_(cat);
				printf("Album added successfully! Add more ('exit' to quit): ");
			} else {
				//printf("\nPress any key to exit..\n");
				break;
				//return 0;
			}
		}
	}
}

void show_artists_(struct Catalog *cat) {
	int i = 0;
	if(cat->art_count > 0) {
		for(i=0; i<cat->art_count; i++) {
			printf("%d) %s\n", i+1, cat->artists[i].name);
		}
	} else {
		printf("No artists created yet!\n");
	}
}

int album_exists_(struct Catalog *cat, char *album_name) {
	int i = 0;
	int exists = 0;
	for(i=0; i<cat->alb_count; i++) {
		if(strcmp(cat->albums[i].name, album_name) == 0) {
			exists = 1;
			cat->alb_current_key = i;
			break;
		}
	}
	return exists;
}

int get_album_key_(struct Catalog *cat, char *album_name) {
	int i = 0;
	int key = -1;
	for(i=0; i<cat->alb_count; i++) {
		if(strcmp(cat->albums[i].name, album_name) == 0) {
			key = i;
			break;
		}
	}
	if(key > -1)
		cat->alb_current_key = key;
	return key;
}

int artist_exists_(struct Catalog *cat, char *name) {
	int i = 0;
	int exists = 0;
	for(i=0; i<cat->art_count; i++) {
		if(strcmp(cat->artists[i].name, name) == 0) {
			exists = 1;
			cat->art_current_key = i;
			break;
		}
	}
	return exists;
}

void add_artist_(struct Catalog *cat) {
	FILE *fp;
	char input[50];
	char sel;
	int key;
	char albumname[50];
	char artstname[50];
	printf("Add a new artist (type 'exit' to quit): ");
	while(fgets(input, 50, stdin)) {
		strip_newline(input, 50);
		if(strlen(input) > 0) {
			if(strcmp(input, "exit") != 0) {
				if(!artist_exists_(cat, input)) {
					strcpy(cat->artists[cat->art_count].name, input);
					fp = fopen("artists.txt", "a+");
					if(fp) {
						fprintf(fp, "%s\n", cat->artists[cat->art_count].name);
						fclose(fp);
					}

					memset(artstname, 0, sizeof(artstname));
					strcat(artstname, input);
					artstname[strlen(artstname)+1] = '\0';
					fp = fopen(artstname, "w");
					if(fp) {
						fprintf(fp, "Name: %s\n", input);
						fclose(fp);
					}

					printf("Enter an album: ");
					// Check if album exists in the array of char pointers => **album_names;

					memset(input, 0, sizeof(input));
					fgets(input, 50, stdin);
					strip_newline(input, 50);
					if(strlen(input) > 0) {
						if(!album_exists_(cat, input)) {
							// Add an album to main albums and increment album count, then map albumkey to artist alb_count and increment it too.
							strip_newline(input, 50);
							if(strlen(input) > 0) {
								if(strcmp(input, "exit") != 0) {
									strcpy(cat->albums[cat->alb_count].name, input);
									strcpy(cat->albums[cat->alb_count].artst.name, cat->artists[cat->art_count].name);
									fp = fopen("albums.txt", "a+");
									if(fp) {
										fprintf(fp, "%s\n", cat->albums[cat->alb_count].name);
										fclose(fp);
									}

									// Create album file for adding information
									memset(albumname, 0, sizeof(albumname));
									strcat(albumname, cat->albums[cat->alb_count].name);
									strcat(albumname, ".txt");
									albumname[strlen(albumname)+1] = '\0';
									fp = fopen(albumname, "w");

									if(fp) {
										fprintf(fp, "Artist: %s\n", cat->artists[cat->art_count].name);
										fclose(fp);

										strcpy(cat->artists[cat->art_count].name, input);
										cat->artists[cat->art_count].albumids[cat->artists[cat->art_count].alb_count] = cat->alb_count;
										++cat->artists[cat->art_count].alb_count;
										fp = fopen("artists.txt", "a+");
										if(fp) {
											fprintf(fp, "%s\n", cat->artists[cat->art_count].name);
											fclose(fp);
										}
										cat->art_current_key = cat->art_count;
										//++cat->art_count;
									}

									printf("Enter the album's release date (dd-mm-yy): ");
									memset(input, 0, sizeof(input));
									fgets(input, 50, stdin);
									strip_newline(input, 50);
									if(strlen(input) > 0) {
										
										if(strcmp(input, "exit") != 0) {
											strcpy(cat->albums[cat->alb_count].release_date, input);
											fp = fopen(albumname, "a+");
											if(fp) {
												fprintf(fp, "Release date: %s\n", cat->albums[cat->alb_count].release_date);
												fclose(fp);

											}
										}
									}

									key = get_album_key_(cat, input);
									if(key > -1) { // We map the artist to the album in memory
										cat->artists[cat->art_count].albumids[cat->artists[cat->art_count].alb_count] = key;
										++cat->artists[cat->art_count].alb_count;

										/** Map album to artist in file */
										fp = fopen(artstname, "a+");
										if(fp) {
											fprintf(fp, "Album ID: %d\n", key);
											fclose(fp);
										}
									}
									cat->alb_current_key = cat->alb_count;
									++cat->alb_count;
								}
							}
						} else { // Album already exists. We can map it.
							printf("Album name already exists. Do you want to map it to this artist? (y/n): ");
							scanf("%c", &sel);
							getchar();
							sel = toupper(sel);
							/** Mapping is done by having an integer array in the artist struct that contains albumids from catalog struct to have all his albums at one place. */
							switch(sel) {
								case 'Y':
									key = get_album_key_(cat, input);
									if(key > -1) {
										cat->artists[cat->art_count].albumids[cat->artists[cat->art_count].alb_count] = key; // Map album to artist in memory
										++cat->artists[cat->art_count].alb_count;

										fp = fopen(artstname, "a+");
										if(fp) {
											fprintf(fp, "Album ID: %d\n", key); // Map album to artist in file
											fclose(fp);
										}
									} else {
										printf("Error mapping key to artist!\n");
									}
									break;
								case 'N':
									break;
								default:
									printf("Invalid selection!\n");
									break;
							}
						}
					}
					cat->art_current_key = cat->art_count;
					++cat->art_count;
					show_artists_(cat);
					printf("Artist added successfully! Add more ('exit' to quit): ");
				} else {
					printf("Artist already exists with that name! Add another ('exit' to quit): ");
				}
			} else {
				break;
			}
		}
	}
}

void update_albums_file_(struct Catalog *cat) {
	FILE *fp;
	int i;
	fp = fopen("albums.txt", "w");
	if(fp) {
		for(i=0; i<cat->alb_count; i++) {
			fprintf(fp, "%s\n", cat->albums[i].name);
		}
		fclose(fp);
	}
}

int album_key_exists_(struct Catalog *cat, int artkey, int albkey) {
	int i = 0;
	int exists = 0;
	for(i=0; i<cat->artists[artkey].alb_count; i++) {
		if(albkey == cat->artists[artkey].albumids[i]) {
			exists = 1;
			break;
		}
	}
	return exists;
}

void show_album_info_(struct Catalog *cat, int albkey) {
	char albname[50];
	char artstname[50];
	char release_dt[50];
	strcpy(albname, cat->albums[albkey].name);
	strcpy(artstname, cat->albums[albkey].artst.name);
	strcpy(release_dt, cat->albums[albkey].release_date);

	printf("Artist: %s\n", artstname);
	printf("Release date: %s\n", release_dt);
}

void edit_albums_(struct Catalog *cat) {
	int sel;
	char input[50];
	char albumname[30];
	char oldalbumname[30];
	int alb_edited = 0;

	char artstname[50];
	int artcount = 0;
	int albcount;
	int key = -1;
	int artexists = 0;
	int artkey;
	int albkey;
	FILE *fp;
	if(cat->alb_count > 0) {
		printf("Enter the album number to edit (0) to quit: ");
		scanf("%d", &sel);
		getchar();
		if(sel > cat->alb_count || sel <= 0) {
			printf("Invalid selection!");
		} else {
			printf("Editing: %s\n", cat->albums[sel-1].name);

			albkey = sel-1;
			show_album_info_(cat, albkey);

			printf("Type new name or enter 'exit' to skip: ");
			fgets(input, 50, stdin);
			
			/** Try getting old album data to unlink that file */
			memset(oldalbumname, 0, sizeof(oldalbumname));
			strcat(oldalbumname, cat->albums[sel-1].name);
			strcat(oldalbumname, ".txt");
			oldalbumname[strlen(oldalbumname)+1] = '\0';

			strip_newline(input, 50);
			if(strlen(input) > 0) {
				
				if(strcmp(input, "exit") != 0) {
					cat->albums[sel-1].name[0] = '\0';
					strcpy(cat->albums[sel-1].name, input);

					memset(albumname, 0, sizeof(albumname));
					strcat(albumname, cat->albums[sel-1].name);
					strcat(albumname, ".txt");
					albumname[strlen(albumname)+1] = '\0';
					alb_edited = 1;

					unlink(oldalbumname);

				}

				printf("Enter the artist name ('exit' to skip): ");
				memset(input, 0, sizeof(input));
				fgets(input, 50, stdin);
				strip_newline(input, 50);
				if(strlen(input) > 0) {
					
					if(strcmp(input, "exit") != 0) {
						/** Update album information about artist */
						strcpy(cat->albums[sel-1].artst.name, input);

						// Getting key using 
						artkey = get_artist_key_(cat, input);

						/** Check artist data outside album */
						if(!artist_exists_(cat, input)) { // Create new artist if not exists
							strcpy(cat->artists[cat->art_count].name, input);
							
						} else {
							/** Artist with name exists */

							artexists = 1;
							

						}

						artcount = cat->art_count;

						/** Mapping album data to artist in memory */
						if(albkey > -1) {
							if(!album_key_exists_(cat, artkey, albkey)) {
								cat->artists[artcount].albumids[cat->artists[artcount].alb_count] = albkey;
								++cat->artists[artcount].alb_count;
							}
						}

						if(artexists == 0) {
							/** Inserting new artist name to artists txt file */
							fp = fopen("artists.txt", "a+"); // This info is going to be overwritten anyways
							if(fp) {
								fprintf(fp, "%s\n", cat->artists[cat->art_count].name);
								fclose(fp);
							}
							++cat->art_count;
						}

						memset(artstname, 0, sizeof(artstname));
						strcat(artstname, input);
						strcat(artstname, ".txt");
						artstname[strlen(artstname)+1] = '\0';

						//printf("Input: %s, Artist name: %s\n", input, artstname);

						if(artexists == 0) { // New artist, file create fresh and append name
							fp = fopen(artstname, "w");
							if(fp) {
								fprintf(fp, "Name: %s\n", input);
								fclose(fp);
							}
						}

						/** We have to check if album key already exists for artist and do not append it if it exists. */
						
						if(!album_key_exists_(cat, artkey, albkey)) {
							fp = fopen(artstname, "a+");
							if(fp) {
								fprintf(fp, "Album ID: %d\n", albkey); // Assign album key to artist in file
								fclose(fp);
							}
						}

					}

					if(alb_edited == 1) {
						fp = fopen(albumname, "w");
					} else {
						fp = fopen(oldalbumname, "w");
					}
					
					if(fp) {
						fprintf(fp, "Artist: %s\n", cat->albums[sel-1].artst.name);
						fclose(fp);
					}
					

					artcount = cat->art_count;
				}

				printf("Enter the album's release date (dd-mm-yy) - Type 'exit' to skip: ");
				memset(input, 0, sizeof(input));
				fgets(input, 50, stdin);
				strip_newline(input, 50);
				if(strlen(input) > 0) {

					if(strcmp(input, "exit") != 0) {
						strcpy(cat->albums[sel-1].release_date, input);
						
					}

					if(alb_edited ==  1) {
						fp = fopen(albumname, "a+");
					} else {
						fp = fopen(oldalbumname, "a+");
					}

					if(fp) {
						fprintf(fp, "Release date: %s\n", cat->albums[sel-1].release_date);
						fclose(fp);
					}
					
				}
				printf("Album '%s' successfully edited!\n", cat->albums[sel-1].name);
				update_albums_file_(cat);
			}
		}
	}
}

void update_artists_file_(struct Catalog *cat) {
	FILE *fp;
	int i;
	fp = fopen("artists.txt", "w");
	if(fp) {
		for(i=0; i<cat->art_count; i++) {
			fprintf(fp, "%s\n", cat->artists[i].name);
		}
		fclose(fp);
	}
}

void show_artist_info_(struct Catalog *cat, int artkey) {
	int i = 0;
	int key = -1;
	char albumname[40];
	printf("Name: %s\n", cat->artists[artkey].name);
	while(i < cat->artists[artkey].alb_count) { // Remember the value of albumids[] array key is in turn the key for albums
		//printf("%d-%d\n", i, cat->artists[artkey].albumids[i]);
		key = -1;
		key = cat->artists[artkey].albumids[i];
		if(key > -1) {
			memset(albumname, 0, sizeof(albumname));
			strcpy(albumname, cat->albums[key].name);
			printf("Album: %s\n", albumname);
		}
		++i;
	}

}

void edit_artists_(struct Catalog *cat) {
	int sel;
	char input[50];
	char artstname[50];
	char oldartstname[50];
	char albumname[50];
	FILE *fp;
	int i;
	int key;
	if(cat->art_count > 0) {
		printf("Enter the artist number to edit (0) to quit: ");
		scanf("%d", &sel);
		getchar();
		if(sel > cat->art_count || sel <= 0) {
			printf("Invalid selection!");
		} else {
			printf("Editing: %s\n", cat->artists[sel-1].name);

			show_artist_info_(cat, sel-1);

			printf("Type new name to enter or 'exit' to quit: ");
			fgets(input, 50, stdin);

			memset(artstname, 0, sizeof(artstname));
			memset(oldartstname, 0, sizeof(oldartstname));
			strcat(oldartstname, cat->artists[sel-1].name);
			strcat(oldartstname, ".txt");
			oldartstname[strlen(oldartstname)+1] = '\0';

			
			strip_newline(input, 50);
			if(strlen(input) > 0) {
				
				if(strcmp(input, "exit") != 0) {
					if(!artist_exists_(cat, input)) { // If the name isn't found in the list of artists we already have
						printf("New artist file creation underway..\n");

						/** Update artist names of all albums with the artist associated */
						/* This is for memory purpose */

						i = 0;
						//for(i=0; i<cat->artists[sel-1].alb_count; i++) {
						//printf("Total: %d\n", cat->artists[sel-1].alb_count);

						/** Update album ids in file */


						/**  */


						strcat(artstname, input);
						strcat(artstname, ".txt");
						artstname[strlen(artstname)+1] = '\0';

						/** New artist file */
						fp = fopen(artstname, "w");

						/* Delete old artist name file */
						unlink(oldartstname);

						// As we create new artist file name, we append the old album ids he had
						/*printf("Total: %d\n", cat->artists[sel-1].alb_count);
						printf("First: %d\n", cat->artists[sel-1].albumids[0]);*/
						if(fp) {
							fprintf(fp, "Name: %s\n", input);
							while(i < cat->artists[sel-1].alb_count) {
								key = -1;
								//printf("Previous album id: %d\n", cat->artists[sel-1].albumids[i]);
								//key = i;
								//printf("%s\n", cat->albums[cat->artists[sel-1].albumids[i]].name);
								//printf("%d\n", i);
								key = cat->artists[sel-1].albumids[i];
								if(key > -1) {
									memset(cat->albums[key].artst.name, 0, sizeof(cat->albums[key].artst.name));
									strcpy(cat->albums[key].artst.name, cat->artists[sel-1].name);
									fprintf(fp, "Album ID: %d\n", key);
								}
								++i;
							}
							fclose(fp);
						}
					} else { // Artist already exists with provided name
						key = -1;
						i = 0;
						// We get key of the existing artist in cat struct list of artists
						// We append the album ids of old artist to this new artist
						key = get_artist_key_(cat, input);
						if(key > -1) {
							fp = fopen(artstname, "a+");
							if(fp) {
								while(i < cat->artists[sel-1].alb_count) {
									fprintf(fp, "Album ID: %d\n", key);
									++i;
								}
								fclose(fp);
							}
						}

					}

					cat->artists[sel-1].name[0] = '\0'; // Clear memory for that stack
					strcpy(cat->artists[sel-1].name, input);
					printf("Artist name successfully edited to: %s\n", cat->artists[sel-1].name);
					update_artists_file_(cat);
					//getchar();


					/** Update album file and album data with updated artist information.

					// TODO CODE
					// Pick all albums with this artist name
					// Edit the album file

					// TODO CODE
					// Function to get all album keys of that artist
					

					*/
					i = 0;

					// Recreate all album files with new artist name
					while(i < cat->artists[sel-1].alb_count) {
						key = -1;
						key = cat->artists[sel-1].albumids[i];
						if(key > -1) {
							printf("%s\n", cat->albums[key].name);
							memset(albumname, 0, sizeof(albumname));
							strcat(albumname, cat->albums[key].name);
							strcat(albumname, ".txt");
							albumname[strlen(albumname)+1] = '\0';

							/** Update in memory */
							cat->albums[key].artst.name[0] = '\0';
							strcpy(cat->albums[key].artst.name, cat->artists[sel-1].name);

							/** Update in file */
							fp = fopen(albumname, "w");
							if(fp) {
								fprintf(fp, "Artist: %s\n", cat->artists[sel-1].name);
								fprintf(fp, "Release date: %s\n", cat->albums[key].release_date);
								fclose(fp);
							}
						}
						++i;
					}
				}
			}
		}
	}
}

void start_(struct Catalog *cat) {
	char sel;

	//printf("\nChoose the following options: ");
	printf("\nType 'A' to add albums");
	printf(" or type 'B' to add artists");
	printf("\nType 'L' to list albums");
	printf(" or type 'S' to list artists");
	printf("\nType 'X' to exit\n");
	printf("\nEnter selection: ");
	scanf("%c", &sel);
	getchar();
	sel = toupper(sel);
	switch(sel) {
		case 'A':
			add_album_(cat);
			start_(cat);
			break;
		case 'B':
			add_artist_(cat);
			start_(cat);
			break;
		case 'L':
			show_albums_(cat);
			edit_albums_(cat);
			start_(cat);
			break;
		case 'S':
			show_artists_(cat);
			edit_artists_(cat);
			start_(cat);
			break;
		case 'X':
			printf("Press Enter/Return key to exit..");
			break;
		default:
			printf("Invalid selection!");
			break;
	}
}

void init(struct Catalog *cat) {
	char line[50];
	FILE *fp;
	FILE *fp2;
	char albumname[30];
	char artistline[50];
	char artstname[50];
	char artstln[50];
	char *token;
	int i;
	int loaded = 0;

	// Before any function call variables must be instantiated in Microsoft C compiler
	fp = fopen("albums.txt", "r");
	memset(line, 0, sizeof(line));

	/** If file exists, preload all variables and structs with file data */
	// Albums pre-load
	if(fp) {
		while(fgets(line, 50, fp)) {
			strip_newline(line, 50);
			if(strlen(line) > 0) {
				strcpy(cat->albums[cat->alb_count].name, line);
				SendMessage(hAlbListBox, LB_ADDSTRING, 1, (LPARAM) line);

				memset(albumname, 0, sizeof(albumname));
				strcat(albumname, line);
				strcat(albumname, ".txt");
				
				albumname[strlen(albumname)+1] = '\0';
				fp2 = fopen(albumname, "r");
				if(fp2) {
					while(fgets(artistline, 30, fp2)) { // First line is always artist information line
						if(strlen(artistline) > 0) {
							//printf("Length: %d\n", strlen(artistline));
							
							token = (char *) strtok(artistline, ":");
							if(strcmp(token, "Artist") == 0) {
								token = (char *) strtok(NULL, ":");

								if(token != NULL) {

									strip_newline(token, 30);
									if(strlen(token) > 0) {								

										strcpy(cat->albums[cat->alb_count].artst.name, token);
									}
								}
							} else if(strcmp(token, "Release date") == 0) {
								token = (char *) strtok(NULL, ":");

								if(token != NULL) {
									strip_newline(token, 30);
									if(strlen(token) > 0) {
										strcpy(cat->albums[cat->alb_count].release_date, token);
									}
								} // Release date loaded up successfully
							}
						}
					}
					fclose(fp2);
				}
				cat->alb_current_key = cat->alb_count;
				++cat->alb_count;
			}
			++loaded;
		}
		
		fclose(fp);
	}
	memset(line, 0, sizeof(line));

	// Artists pre-load
	fp = fopen("artists.txt", "r");
	if(fp) {
		while(fgets(line, 50, fp)) { // Load each artist name from file
			strip_newline(line, 50);

			if(strlen(line) > 0) {
				SendMessage(hArtListBox, LB_ADDSTRING, 1, (LPARAM) line);
				memset(artstname, 0, sizeof(artstname));
				strcat(artstname, line);
				//printf("%s: ", artstname);
				strcat(artstname, ".txt");
				artstname[strlen(artstname)+1] = '\0';

				fp2 = fopen(artstname, "r"); // Try read each artist file
				if(fp2) { // If file exists and we could read
					i = 0;
					cat->artists[cat->art_count].albumids[0] = -1;
					while(fgets(artstln, 50, fp2)) { // Try read each line of artist file
						if(i > 0) { // We want line greater than 0 because first line is for artist name
							if(strlen(artstln) > 0) {
								token = (char *) strtok(artstln, ":"); // Pull out tokens from each line

								if(strcmp(token, "Album ID") == 0) {
									token = (char *) strtok(NULL, ":"); // First line will not bring a token at all
									
									if(token != NULL) {
										if(strlen(token) > 0) {
											//printf("%s", token);
											strip_newline(token, 50);
											cat->artists[cat->art_count].albumids[i] = atoi(token); // Assign the second token which is ID
										}
									}
								}
							}
						}
						++i;
					}
					cat->artists[cat->art_count].alb_count = i;
					fclose(fp2);
				}
				//printf("\n");
				strcpy(cat->artists[cat->art_count].name, line);
				cat->art_current_key = cat->art_count;
				++cat->art_count;
			}
			++loaded;
		}
		
		fclose(fp);

		/** Next, get the albumids of each artist and propogate in his struct information */
	}

	/* Start the program */
	//start_(cat);
	if(loaded > 0) {
		MessageBox(NULL, "Data loaded from text files to memory..", "Notice", MB_ICONINFORMATION | MB_OK);
	}
}

/*int main(int argc, char *argv[], char *env[]) {
	struct Catalog *cat = &catalog;
	cat->alb_count = 0;
	cat->art_count = 0;

	//printf("Welcome to Console catalog..\n");

	/* Initialize any information which is available outside the program */
	//init(cat);


	//printf("You entered: %s", cat->albums[cat->alb_count].name);
	/*getchar();
	return 0;
}*/

LRESULT CALLBACK AddArtWndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam) {
	switch(msg) {
		default:
			return DefWindowProc(hwnd, msg, wParam, lParam);
			break;
	}
	return 0;
}

BOOL SetUpAddArtWindowClass(HINSTANCE hInstance) {
	WNDCLASSEX wc;

	wc.cbSize = sizeof(WNDCLASSEX);
	wc.style = 0;
	wc.lpfnWndProc = AddArtWndProc;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = hInstance;
	wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = (HBRUSH) (COLOR_3DFACE + 1);
	wc.lpszMenuName = NULL;
	wc.lpszClassName = g_szAddClassName;
	wc.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	
	if(!RegisterClassEx(&wc)) {
		MessageBox(NULL, "Failed to register 'add artist' window class!", "Error", MB_ICONEXCLAMATION | MB_OK);
		return FALSE;
	}

	return TRUE;
}

LRESULT CALLBACK AddAlbWndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam) {
	switch(msg) {
		case WM_CREATE:
			{
				hAlbNameLbl = CreateWindowEx(
									0,
									"STATIC",
									"Album name",
									WS_VISIBLE | WS_CHILD | SS_LEFT,
									10, 10, 200, 20,
									hwnd, (HMENU) IDC_ALBNAMELBL, GetModuleHandle(NULL), NULL);
				if(hAlbNameLbl == NULL) {
					MessageBox(hwnd, "Failed to create album name label!", "Error", MB_ICONERROR | MB_OK);
					return -1;
				}

				SendMessage(hAlbNameLbl, WM_SETFONT, (WPARAM) hDefault, MAKELPARAM(0, FALSE));
			}
			{
				hAlbName = CreateWindowEx(
									WS_EX_CLIENTEDGE,
									"EDIT",
									"",
									WS_CHILD | WS_VISIBLE,
									10, 30, 200, 20,
									hwnd, (HMENU) IDC_ALBNAME, GetModuleHandle(NULL), NULL);

				if(hAlbName == NULL) {
					MessageBox(hwnd, "Failed to create album name input control!", "Error", MB_ICONEXCLAMATION | MB_OK);
					return -1;
				}

				SendMessage(hAlbName, WM_SETFONT, (WPARAM) hDefault, MAKELPARAM(0, FALSE));
			}
			{
				hAlbArtNameLbl = CreateWindowEx(
									0,
									"STATIC",
									"Artist name",
									WS_VISIBLE | WS_CHILD | SS_LEFT,
									10, 80, 200, 20,
									hwnd, (HMENU) IDC_ALBARTNAMELBL, GetModuleHandle(NULL), NULL);
				if(hAlbArtNameLbl == NULL) {
					MessageBox(hwnd, "Failed to create album artist label!", "Error", MB_ICONEXCLAMATION | MB_OK);
					return -1;
				}

				SendMessage(hAlbArtNameLbl, WM_SETFONT, (WPARAM) hDefault, MAKELPARAM(0, FALSE));
			}
			{
				hAlbArtName = CreateWindowEx(
									WS_EX_CLIENTEDGE,
									"EDIT",
									"",
									WS_CHILD | WS_VISIBLE,
									10, 100, 200, 20,
									hwnd, (HMENU) IDC_ALBARTNAME, GetModuleHandle(NULL), NULL);
				if(hAlbArtName == NULL) {
					MessageBox(hwnd, "Failed to create album artist input control!", "Error", MB_ICONERROR | MB_OK);
					return -1;
				}

				SendMessage(hAlbArtName, WM_SETFONT, (WPARAM) hDefault, MAKELPARAM(0, FALSE));
			}
			{
				hAlbRelDateLbl = CreateWindowEx(
									0,
									"STATIC",
									"Release date",
									WS_CHILD | WS_VISIBLE | SS_LEFT,
									10, 150, 200, 20,
									hwnd, (HMENU) IDC_ALBRELDATELBL, GetModuleHandle(NULL), NULL);
				if(hAlbRelDateLbl == NULL) {
					MessageBox(hwnd, "Failed to create release date label!", "Error", MB_ICONEXCLAMATION | MB_OK);
					return -1;
				}

				SendMessage(hAlbRelDateLbl, WM_SETFONT, (WPARAM) hDefault, MAKELPARAM(0, FALSE));
			}
			{
				hAlbRelDate = CreateWindowEx(
									WS_EX_CLIENTEDGE,
									"EDIT",
									"",
									WS_CHILD | WS_VISIBLE,
									10, 170, 200, 20,
									hwnd, (HMENU) IDC_ALBRELDATE, GetModuleHandle(NULL), NULL);
				if(hAlbRelDate == NULL) {
					MessageBox(hwnd, "Failed to create release date input control!", "Error", MB_ICONERROR | MB_OK);
					return -1;
				}

				SendMessage(hAlbRelDate, WM_SETFONT, (WPARAM) hDefault, MAKELPARAM(0, FALSE));
			}
			{
				hAlbAddButton = CreateWindowEx(
									WS_EX_CLIENTEDGE,
									"BUTTON",
									"Add album",
									WS_CHILD | WS_VISIBLE,
									220, 160, 90, 30,
									hwnd, (HMENU) IDC_ALBADDBTN, GetModuleHandle(NULL), NULL);
				if(hAlbAddButton == NULL) {
					MessageBox(hwnd, "Failed to create add album button!", "Error", MB_ICONEXCLAMATION | MB_OK);
					return -1;
				}
				
				SendMessage(hAlbAddButton, WM_SETFONT, (WPARAM) hDefault, MAKELPARAM(0, FALSE));
			}
			break;
		case IDC_ALBADDBTN:
			break;
		default:
			return DefWindowProc(hwnd, msg, wParam, lParam);
			break;
	}
	return 0;
}

BOOL SetUpAddAlbWindowClass(HINSTANCE hInstance) {
	WNDCLASSEX wc;

	wc.cbSize = sizeof(WNDCLASSEX);
	wc.style = CS_HREDRAW | CS_VREDRAW;
	wc.lpfnWndProc = AddAlbWndProc;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = hInstance;
	wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = (HBRUSH) (COLOR_3DFACE + 1);
	wc.lpszMenuName = NULL;
	wc.lpszClassName = g_szAddClassName;
	wc.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	if(!RegisterClassEx(&wc)) {
		MessageBox(NULL, "Failed to register 'Add window' class!", "Error", MB_ICONEXCLAMATION | MB_OK);
		return FALSE;
	}
	return TRUE;
}


LRESULT CALLBACK WndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam) {
	switch(msg) {
		case WM_CREATE:
			{
				struct Catalog *cat = &catalog;
				cat->alb_count = 0;
				cat->art_count = 0;

				//printf("Welcome to Console catalog..\n");

				/* Initialize any information which is available outside the program */
				init(cat);
			}
			{
				g_hbrBackground = CreateSolidBrush(RGB(255, 255, 255));
			}
			{
				hDefault = GetStockObject(DEFAULT_GUI_FONT);
			}
			{
				hAlbLabel = CreateWindowEx(
									0,
									"STATIC",
									"Albums",
									WS_CHILD | WS_VISIBLE | SS_LEFT,
									20, 10, 200, 30,
									hwnd, (HMENU) IDC_ALBLABEL, GetModuleHandle(NULL), NULL);

				if(hAlbLabel == NULL) {
					MessageBox(hwnd, "Failed to create albums label!", "Error", MB_ICONEXCLAMATION | MB_OK);
					return -1;
				}
				SendMessage(hAlbLabel, WM_SETFONT, (WPARAM) hDefault, MAKELPARAM(0, FALSE));
			}
			{
				hAlbListBox = CreateWindowEx(
									WS_EX_CLIENTEDGE,
									"LISTBOX",
									"",
									WS_CHILD | WS_VISIBLE | LBS_STANDARD,
									20, 50, 250, 300,
									hwnd, (HMENU) IDC_ALBLISTBOX, GetModuleHandle(NULL), NULL);

				if(hAlbListBox == NULL) {
					MessageBox(hwnd, "Failed to create albums listbox!", "Error", MB_ICONEXCLAMATION | MB_OK);
					return -1;
				}
				/*SendMessage(hAlbListBox, LB_ADDSTRING, 0, (LPARAM) "Dangerous");
				SendMessage(hAlbListBox, LB_ADDSTRING, 1, (LPARAM) "Thriller");
				SendMessage(hAlbListBox, LB_ADDSTRING, 2, (LPARAM) "Invincible");
				SendMessage(hAlbListBox, LB_ADDSTRING, 4, (LPARAM) "All Rise");*/

				SendMessage(hAlbListBox, WM_SETFONT, (WPARAM) hDefault, MAKELPARAM(0, FALSE));
			}
			{
				hAlbAddBtn = CreateWindowEx(
									WS_EX_CLIENTEDGE,
									"BUTTON",
									"Add Album",
									WS_CHILD | WS_VISIBLE,
									20, 370, 95, 25,
									hwnd, (HMENU) IDC_ADDALBBTN, GetModuleHandle(NULL), NULL);
				if(hAlbAddBtn == NULL) {
					MessageBox(hwnd, "Failed to create album add button!", "Error", MB_ICONEXCLAMATION | MB_OK);
					return -1;
				}
				
				SendMessage(hAlbAddBtn, WM_SETFONT, (WPARAM) hDefault, MAKELPARAM(0, FALSE));
			}
			{
				hAlbDetBtn = CreateWindowEx(
									WS_EX_CLIENTEDGE,
									"BUTTON",
									"Album details",
									WS_CHILD | WS_VISIBLE,
									120, 370, 95, 25,
									hwnd, (HMENU) IDC_ALBDETBTN, GetModuleHandle(NULL), NULL);
				if(hAlbDetBtn == NULL) {
					MessageBox(hwnd, "Failed to create album details button!", "Error", MB_ICONEXCLAMATION | MB_OK);
					return -1;
				}

				SendMessage(hAlbDetBtn, WM_SETFONT, (WPARAM) hDefault, MAKELPARAM(0, FALSE));
			}
			{
				hArtLabel = CreateWindowEx(
									0,
									"STATIC",
									"Artists",
									WS_CHILD | WS_VISIBLE | SS_LEFT,
									350, 10, 200, 30,
									hwnd, (HMENU) IDC_ARTLABEL, GetModuleHandle(NULL), NULL);

				if(hArtLabel == NULL) {
					MessageBox(hwnd, "Failed to create artists label!", "Error", MB_ICONEXCLAMATION | MB_OK);
					return -1;
				}
				SendMessage(hArtLabel, WM_SETFONT, (WPARAM) hDefault, MAKELPARAM(0, FALSE));
			}
			{
				hArtListBox = CreateWindowEx(
									WS_EX_CLIENTEDGE,
									"LISTBOX",
									"",
									WS_CHILD | WS_VISIBLE | LBS_STANDARD,
									350, 50, 250, 300,
									hwnd, (HMENU) IDC_ARTLISTBOX, GetModuleHandle(NULL), NULL);

				if(hArtListBox == NULL) {
					MessageBox(hwnd, "Failed to create artists listbox!", "Error", MB_ICONEXCLAMATION | MB_OK);
					return -1;
				}

				/*SendMessage(hArtListBox, LB_ADDSTRING, 0, (LPARAM) "A.R Rahman");
				SendMessage(hArtListBox, LB_ADDSTRING, 1, (LPARAM) "Britney Spears");
				SendMessage(hArtListBox, LB_ADDSTRING, 2, (LPARAM) "Michael Jackson");*/

				SendMessage(hArtListBox, WM_SETFONT, (WPARAM) hDefault, MAKELPARAM(0, FALSE));
			}
			{
				hArtAddBtn = CreateWindowEx(
									WS_EX_CLIENTEDGE,
									"BUTTON",
									"Add artist",
									WS_CHILD | WS_VISIBLE,
									350, 370, 95, 25,
									hwnd, (HMENU) IDC_ADDARTBTN, GetModuleHandle(NULL), NULL);
				if(hArtAddBtn == NULL) {
					MessageBox(hwnd, "Failed to create add artist button!", "Error", MB_ICONEXCLAMATION | MB_OK);
					return -1;
				}

				SendMessage(hArtAddBtn, WM_SETFONT, (WPARAM) hDefault, MAKELPARAM(0, FALSE));
			}
			{
				hArtDetBtn = CreateWindowEx(
									WS_EX_CLIENTEDGE,
									"BUTTON",
									"Artist details",
									WS_CHILD | WS_VISIBLE,
									450, 370, 95, 25,
									hwnd, (HMENU) IDC_ARTDETBTN, GetModuleHandle(NULL), NULL);
				if(hArtDetBtn == NULL) {
					MessageBox(hwnd, "Failed to create artist details button!", "Error", MB_ICONEXCLAMATION | MB_OK);
					return -1;
				}
				
				SendMessage(hArtDetBtn, WM_SETFONT, (WPARAM) hDefault, MAKELPARAM(0, FALSE));
			}
			break;
		case WM_COMMAND:
			switch(LOWORD(wParam)) {
				case IDC_ADDALBBTN:
					{
						hAddAlbWnd = CreateWindowEx(
												WS_EX_CLIENTEDGE,
												g_szAddClassName,
												"Add an album",
												WS_OVERLAPPEDWINDOW,
												CW_USEDEFAULT, CW_USEDEFAULT, 400, 250,
												NULL, NULL, GetModuleHandle(NULL), NULL);

						if(hAddAlbWnd == NULL) {
							MessageBox(hwnd, "Failed to create 'Add album' window!", "Error", MB_ICONEXCLAMATION | MB_OK);
							return -1;
						}

						ShowWindow(hAddAlbWnd, SW_SHOW);
						UpdateWindow(hAddAlbWnd);
					}
					break;
				case IDC_ADDARTBTN:
					{
						hAddArtWnd = CreateWindowEx(
												WS_EX_CLIENTEDGE,
												g_szAddClassName,
												"Add an artist",
												WS_OVERLAPPEDWINDOW,
												CW_USEDEFAULT, CW_USEDEFAULT, 400, 250,
												NULL, NULL, GetModuleHandle(NULL), NULL);

						if(hAddArtWnd == NULL) {
							MessageBox(hwnd, "Failed to create 'Add artist' window!", "Error", MB_ICONERROR | MB_OK);
							return -1;
						}

						ShowWindow(hAddArtWnd, SW_SHOW);
						UpdateWindow(hAddArtWnd);
					}
					break;
			}
			break;
		case WM_CTLCOLORSTATIC:
			{
				HDC hdcStatic = (HDC) wParam;
				SetBkMode(hdcStatic, TRANSPARENT);
				SetTextColor(hdcStatic, RGB(0, 0, 0));
				return (LONG) g_hbrBackground;
			}
			break;
		case WM_CTLCOLORDLG:
			return (LONG) g_hbrBackground;
			break;
		case WM_CLOSE:
			DestroyWindow(hwnd);
			break;
		case WM_DESTROY:
			PostQuitMessage(0);
			break;
		default:
			return DefWindowProc(hwnd, msg, wParam, lParam);
			break;
	}
	return 0;
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow) {
	WNDCLASSEX wc;
	HWND hwnd;
	MSG Msg;

	wc.cbSize = sizeof(WNDCLASSEX);
	wc.style = 0;
	wc.lpfnWndProc = WndProc;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = hInstance;
	wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = (HBRUSH) (COLOR_WINDOW + 1);
	wc.lpszMenuName = NULL;
	wc.lpszClassName = g_szClassName;
	wc.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	if(!RegisterClassEx(&wc)) {
		MessageBox(NULL, "Failed to register window class!", "Error", MB_ICONEXCLAMATION | MB_OK);
		return -1;
	}

	hwnd = CreateWindowEx(
					WS_EX_CLIENTEDGE,
					g_szClassName,
					"Welcome to Music CD Catalog",
					WS_OVERLAPPEDWINDOW,
					CW_USEDEFAULT, CW_USEDEFAULT, 650, 500,
					NULL, NULL, hInstance, NULL);

	if(hwnd == NULL) {
		MessageBox(NULL, "Failed to create main window!", "Error", MB_ICONEXCLAMATION | MB_OK);
		return -1;
	}

	ShowWindow(hwnd, nCmdShow);
	UpdateWindow(hwnd);

	g_hMainWindow = hwnd;
	if(!SetUpAddAlbWindowClass(hInstance)) {
		MessageBox(hwnd, "Failed to set up 'Add album' window class!", "Error", MB_ICONEXCLAMATION | MB_OK);
		return -1;
	}

	while(GetMessage(&Msg, NULL, 0, 0) > 0) {
		TranslateMessage(&Msg);
		DispatchMessage(&Msg);
	}

	return Msg.wParam;
}
